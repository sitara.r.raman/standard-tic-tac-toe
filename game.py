def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board, player):
    print_board(board)
    print(player, "has won")
    print("Game Over")
    exit()

def is_row_winner(board, row_number):
    start_index = (row_number-1) * 3
    if (board[start_index] == board[start_index + 1] and board[start_index + 1] == board[start_index + 2]):
        return True

def is_column_winner(board, column_number):
    start_index = column_number-1
    if (board[start_index] == board[start_index + 3] and board[start_index + 3] == board[start_index + 6]):
        return True

def is_diagonal_winner(board, diagonal_number):
    if (diagonal_number == 1):
        start_index = 0
        add_index = 4
    elif (diagonal_number == 2):
        start_index = 2
        add_index = 2
    if (board[start_index] == board[start_index + add_index] and board[add_index] == board[start_index + (add_index * 2)]):
        return True


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    while(1):
        response = input("Where would " + current_player + " like to move? ")
        space_number = int(response) - 1
        if (space_number > 8 or space_number < 0):
            print ("Error, enter a number between 1 and 9")
        elif (board[space_number] == "X" or board[space_number] == "O"):
            print("The space is occupied")
        else:
            break
    board[space_number] = current_player

    if (is_row_winner(board, 1) == True):
        game_over(board, current_player)

    elif (is_row_winner(board, 2) == True):
        game_over(board, current_player)

    elif (is_row_winner(board, 3) == True):
        game_over(board, current_player)

    elif (is_column_winner(board, 1) == True):
        game_over(board, current_player)

    elif (is_column_winner(board, 2) == True):
        game_over(board, current_player)

    elif (is_column_winner(board, 3) == True):
        game_over(board, current_player)

    elif (is_diagonal_winner(board, 1) == True):
        game_over(board, current_player)

    elif (is_diagonal_winner(board, 2) == True):
        game_over(board, current_player)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
